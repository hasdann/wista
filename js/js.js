$(document).foundation()

window.addEventListener("resize", destroySticky);

function destroySticky() {
	var widthWindow = $(window).width();
	if(widthWindow < 1025){
		$('#sticky').foundation('destroy');
	}
}
